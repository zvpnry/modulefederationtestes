import React from "react";


const App = ({ children }) => {
	return (
		<div style={{
			margin: "10px",
			padding: "10px",
			textAlign: "center",
			backgroundColor: "cyan"
		}}>
			<h1 >App Components</h1>
			{children}
		</div>
	)
}

export default App;

