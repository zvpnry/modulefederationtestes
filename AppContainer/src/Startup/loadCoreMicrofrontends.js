const loadScript = (url) => new Promise((resolve, reject) => {
	const script = document.createElement('script');
	script.src = url;
	script.onload = () => {
		resolve();
	};
	script.onerror = () => {
		reject();
	};
	document.head.appendChild(script);
});

export default () => {
	const promises = [];
	promises.push(loadScript(`http://localhost:3001/remoteEntry.js`));
	// promises.push(loadScript(`${_SELF_CONFIGS.CORE_SHELL_HOST}/configuration/Configurations.js`));
	return Promise.all(promises);
};
