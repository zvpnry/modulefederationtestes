// import InitializeGlobalConfigurations from './InitializeGlobalConfigurations';
import InitializewindowVariables from './InitializewindowVariables';
import loadCoreMicrofrontends from './loadCoreMicrofrontends';

const StartUp = () => {
	InitializewindowVariables();
	return loadCoreMicrofrontends();
	// return InitializeGlobalConfigurations().then(configs => loadCoreMicrofrontends(configs));
};

export default StartUp;
