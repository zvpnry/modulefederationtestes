import _SELF_CONFIGS from '_SELF_CONFIGS';

export default () => fetch(`${_SELF_CONFIGS.CONFIG_MANGER_HOST}/configuration/Configurations.json`)
	.then(res => res.json())
	.then(configs => {
		window.GlobalConfigurations = configs;
		return configs;
	});
