export default () => {
	window.InitializeModuleFederationRemote = (remoteName) => resolve => {
		resolve({
			get: (request) => window[remoteName].get(request),
			init: (arg) => {
				try {
					return window[remoteName].init(arg);
				} catch (e) {
					console.log(`remote ${remoteName} already initialized`);
					return undefined;
				}
			}
		});
	};
};
