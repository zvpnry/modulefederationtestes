import StartUp from './Startup';

StartUp().then(() => {
	import('./bootstrap');
}).catch(err => {
	import('./bootstrapUnavailable');
});
