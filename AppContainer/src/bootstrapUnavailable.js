import React from 'react';
import ReactDOM from 'react-dom';
import Unavailable from './Unavailable';

ReactDOM.render(<Unavailable />, document.getElementById('root'));
