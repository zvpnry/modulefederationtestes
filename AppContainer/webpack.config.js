const HtmlWebpackPlugin = require("html-webpack-plugin");
const { ModuleFederationPlugin } = require("webpack").container;
const ExternalTemplateRemotesPlugin = require("external-remotes-plugin");
const path = require("path");

module.exports = {
	entry: "./src/index",
	mode: "development",
	devServer: {
		static: path.join(__dirname, "dist"),
		port: 4001,
	},
	output: {
		publicPath: "http://localhost:4001/",
	},
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				loader: "babel-loader",
				exclude: /node_modules/,
				options: {
					presets: ["@babel/preset-react"],
				},
			},
		],
	},
	plugins: [
		new ModuleFederationPlugin({
			name: "Container",			
			filename: 'remoteEntry.js',
			exposes:{
				'./App': './src/App.js'
			},
			remotes: {
				AppComponents: 'promise new Promise(window.InitializeModuleFederationRemote("AppComponents"))',
				
			},
			shared: { react: { singleton: true }, "react-dom": { singleton: true } },
		}),
		new ExternalTemplateRemotesPlugin(),
		new HtmlWebpackPlugin({
			template: "./public/index.html",
		}),
	],
};

